import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"; 
import './App.css';
import blog from './blog/blog';
import Post from './blog/Post';
import NoPage from './blog/erreur';
import Navbar from './blog/nav';

function App() {

  return (
    <div className="App">
      
      <Router>
      <Navbar/>
        <Switch>
          <Route exact path="/post/" component={blog} />
          <Route path="/post/:postId" component={Post} />
          <Route component={NoPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
