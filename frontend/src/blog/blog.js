import React, { Component } from 'react'

export default class blog extends Component {
    
    componentDidMount() {
        fetch("http://localhost:3000/posts")
          .then(response => response.json())
          .then(data => {
            this.setState({ data: data });
            console.log(this.state.data);
          });
      }

    render() {

        if(this.state === null){
            return(
                <div>loading...</div>
            )
        } else {
       return(
           <div>
               { this.state.data && this.state.data.map((post, index)=>(
                   <div key={index}>{post.id} {post.content} {post.auteur}</div>
               )) }
           </div>
       )
    }}
}
