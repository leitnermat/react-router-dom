import React from "react";
import { NavLink } from "react-router-dom";
import pages from './page.json';

const activeStyle = {
backgroundColor: "#212121",
color: "white",
margin: '5px',
};

const Navbar = () => (
<nav>
 <NavLink to="/" activeStyle={activeStyle}>
 Home 
 </NavLink>
 <NavLink to="/post/" activeStyle={activeStyle}>
 Blog 
 </NavLink>
 {pages.Pages.map( p => <NavLink key={p} to={'/post/'+p} activeStyle={activeStyle}>
    {p} 
 </NavLink>)}
</nav>
);

export default Navbar