import React, { PureComponent } from 'react'

export default class NoPage extends PureComponent {
    render() {
        return (
            <div>
                Faux !
            </div>
        )
    }
}
