import React, { Component } from 'react'
import Pages from './page.json';

export default class Post extends Component {

    componentDidMount() {
        const { postId } = this.props.match.params;

        if (Pages.Pages.includes(postId)){

        fetch("http://localhost:3000/posts/" + postId)
          .then(response => response.json())
          .then(data => {
            this.setState({ data: data });
            console.log(this.state.data);
          })
          .catch((error) => {
            console.log('error 404');
            throw error;
        });
      }

    }
    componentDidUpdate(){
        const { postId } = this.props.match.params;

        if (Pages.Pages.includes(postId)){

        fetch("http://localhost:3000/posts/" + postId)
          .then(response => response.json())
          .then(data => {
            this.setState({ data: data });
            console.log(this.state.data);
          })
          .catch((error) => {
            console.log('error 404');
            throw error;
        });
    }
    }
    
    render() {

        if(this.state === null){
            return(
                <div>Erreur 404</div>
            )
        } else {
       return(
           <div>
               {this.state.data.id} {this.state.data.content} {this.state.data.auteur}
           </div>
       )
    }}
}
