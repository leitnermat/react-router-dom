# React-router-dom

Ouvrir un terminal et lancer les commandes suivantes:
1) cd backend
2) nodemon index

Ouvrir un second terminal
1) cd frontend 
2) npm start

Tester les URLs qui sont inscrites dans frontend/src/blog/page.json 

localhost:3001/post affiche tous les articles
localhost:3001/ n'existe pas